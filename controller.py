"""Controller of chess."""
import tkinter as tk
from game import Rook, Knight, Bishop, Queen


class Controller:
    """Control moves."""

    def __init__(self):
        """Contruct for controller class."""
        self.start_game()
        self.pos_t = None

    def start_game(self):
        """Start Game."""
        self.position = ({
         "a8": "r", "b8": "n", "c8": "b",  "d8": "q", "e8": "k", 'f8': "b",
         "g8": "n", "h8": "r",
         "a7": "p", "b7": "p", "c7": "p", "d7": "p", "e7": "p", 'f7': "p",
         "g7": "p", "h7": "p",
         "a2": "P", "b2": "P", "c2": "P", "d2": "P", "e2": "P", 'f2': "P",
         "g2": "P", "h2": "P",
         "a1": "R", "b1": "N", "c1": "B", "d1": "Q", "e1": "K", 'f1': "B",
         "g1": "N", "h1": "R",
        })

    def updatePos(self, input, view):
        """Update position."""
        f = input[:2]
        t = input[2:]
        self.en_passant(f, t)
        self.position[t] = self.position.pop(f)
        self.castling_mov(f, t)
        self.promotion(f, t, view)
        return self.position

    def castling_mov(self, f, t):
        """Castling move."""
        c_f, c_t = ord(f[0]), ord(t[0])
        if abs(c_f-c_t) == 2 and self.position[t] in ["K", "k"]:
            x = c_t-1 if c_t > c_f else c_t+1
            rook = "1" if self.position[t] == "K" else "8"
            if (c_t - c_f) > 0:
                self.position[chr(x)+rook] = self.position.pop('h'+rook)
            elif (c_t - c_f) < 0:
                self.position[chr(x)+rook] = self.position.pop('a'+rook)
            return 'castling'

    def en_passant(self, f, t):
        """En passant move."""
        c_f, r_f, c_t, r_t = ord(f[0]), int(f[1]), ord(t[0]), int(t[1])
        piece = self.position.get(f, '  ')
        no_piece = self.position.get(t, '  ')
        r_c = r_t - 1 if piece is 'P' else r_t + 1
        capture = self.position.get(chr(c_t)+str(r_c), ' ')
        if all([piece in ["P", "p"], capture in ["P", "p"], no_piece is '  ',
                abs(r_f-r_t) == 1, abs(c_f - c_t) == 1]):
            self.position.pop(chr(c_t)+str(r_c))
            return 'en_passant'

    def promotion(self, f, t, view):
        """Promotion piece."""
        r_t = int(t[1])
        piece = self.position.get(t, ' ')
        if piece in ['P', 'p'] and r_t in [1, 8]:
            PromotionSelection(view, self.position, t, r_t)
            return 'promotion'

    def construct_from_board(self, board):
        """Undo the last move."""
        chess_b = {}
        for key, value in board.items():
            if value is not None:
                key1 = key[1]+str(key[0]+1)
                value1 = value.name
                chess_b[key1] = value1
        self.position = chess_b
        return self.position


class PromotionSelection(tk.Frame):
    """Dialog box for selecting promotion."""

    def __init__(self, view, position, t, side):
        """Construct for promotion."""
        tk.Frame.__init__(self, view.parent)
        self.view = view
        self.view.parent.withdraw()
        self.parent = self.view.parent
        self.top = tk.Toplevel(self.parent)
        self.position = position
        self.t = t
        self.top.title("Pown Promotion")
        self.image = {}
        self.loadImages()
        self.v = tk.StringVar()
        self.v.set('L')
        self.prom = tk.Frame(self.top, width=64*4, height=64)
        self.prom.pack()
        self.option(side)

    def option(self, side):
        """Select option."""
        labels = "QRNB" if side is 8 else "qrnb"
        for piece in labels:
            b = tk.Radiobutton(self.prom, variable=self.v, value=piece,
                               image=self.image[piece], indicatoron=0)
            b.pack(side='left')
        (tk.Button(self.prom, text='ok', command=self.get_image_tags)
           .pack(anchor='center'))

    def loadImages(self):
        """Load pieace images."""
        for piece in 'nbrqkNBRQK':
            self.image[piece] = (tk.PhotoImage(file='./images/%s.png' % piece))

    def get_image_tags(self, event=None):
        """To get tags."""
        self.position[self.t] = self.v.get()
        self.view.movePiece(self.position)
        row, column = (int(self.t[1])-1, self.t[0])
        self.view.game.cb.board[(row, column)] = self.promotion(self.v.get())
        self.top.destroy()
        self.view.parent.update()
        self.view.parent.deiconify()

    def promotion(self, choice):
        """Promotion."""
        col = 'white' if choice in 'QNBR' else 'black'
        if choice in ['N', 'n']:
            return Knight(col, choice)
        elif choice in ['Q', 'q']:
            return Queen(col, choice)
        elif choice in ['R', 'r']:
            return Rook(col, choice)
        else:
            return Bishop(col, choice)
