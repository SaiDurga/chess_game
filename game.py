"""Implement chess in python."""

import re
import sys
from collections import deque

# %%


class Chess_Board:
    """Write class for chess board."""

    def __init__(self):
        """Create board."""
        self.board = {}
        self.c_w_l = True
        self.c_w_r = True
        self.c_b_l = True
        self.c_b_r = True

    def create_board(self, x=None):
        """Write method to create board."""
        if x is None:
            w_k = King('white', 'K')
            b_k = King('black', 'k')
            for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                self.board[(1, i)] = Pawn('white', 'P')
                self.board[(6, i)] = Pawn('black', 'p')
            self.board[(0, 'a')] = Rook('white', 'R')
            self.board[(0, 'h')] = Rook('white', 'R')
            self.board[(7, 'a')] = Rook('black', 'r')
            self.board[(7, 'h')] = Rook('black', 'r')
            self.board[(0, 'b')] = Knight('white', 'N')
            self.board[(0, 'g')] = Knight('white', 'N')
            self.board[(7, 'b')] = Knight('black', 'n')
            self.board[(7, 'g')] = Knight('black', 'n')
            self.board[(0, 'c')] = Bishop('white', 'B')
            self.board[(0, 'f')] = Bishop('white', 'B')
            self.board[(7, 'c')] = Bishop('black', 'b')
            self.board[(7, 'f')] = Bishop('black', 'b')
            self.board[(0, 'e')] = w_k
            self.board[(0, 'd')] = Queen('white', 'Q')
            self.board[(7, 'e')] = b_k
            self.board[(7, 'd')] = Queen('black', 'q')
        else:
            w_k, b_k = self.set_board_state(x)
        return w_k, b_k

    def set_board_state(self, x):
        """Set board state."""
        self.board = {}
        board = {'p': Pawn, 'r': Rook, 'b': Bishop,
                 'q': Queen, 'n': Knight, 'k': King}
        for key, value in x.items():
            name, color = ((value, 'black') if value.islower()
                           else (str.upper(value), 'white'))
            if color == 'white' and value == 'K':
                k_w = board[str.lower(value)](color, name)
                self.board[key] = k_w
            elif color == 'black' and value == 'k':
                k_b = board[str.lower(value)](color, name)
                self.board[key] = k_b
            else:
                self.board[key] = board[str.lower(value)](color, name)
        return k_w, k_b

    def print_board(self):
        """Print the chess board."""
        print("  a | b | c | d | e | f | g | h |")
        for i in range(7, -1, -1):
            print("-"*32)
            print(i+1, end="|")
            for j in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                item = self.board.get((i, j))
                if item is None:
                    print(" " + ' |', end=" ")
                else:
                    print(str(item.name) + ' |', end=" ")
            print()
        print("-"*32)

    def move(self, col, m, pre_m=None):
        """Move the key."""
        self.col = col
        self.m = m
        d = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}
        x = int(m[1])-1
        y = d[m[0]]
        x1 = int(m[4])-1
        y1 = d[m[3]]
        st_obj = self.board.get((int(m[1])-1, str(m[0])))
        dest_obj = self.board.get((int(m[4])-1, str(m[3])))
        if st_obj is not None:
            if col == st_obj.color:
                is_can = st_obj.valid_moves(x, y, x1, y1, col, self)
                if is_can is True:
                    if dest_obj is None or st_obj.color != dest_obj.color:
                        self.castling_rook_move(is_can, st_obj.name,
                                                x, y, x1, y1)
                        self.board[(int(m[4])-1, m[3])] = st_obj
                        self.board[(int(m[1])-1, m[0])] = None
                        coin = self.board.get((int(m[4])-1, str(m[3]))).name
                        c_w_l, c_w_r, c_b_l, c_b_r = self.check_castling(coin,
                                                                         m)
                    else:
                        return "Cannot replace with same color"
                elif self.en_passant(st_obj, dest_obj, pre_m, x, y, x1, y1):
                    print("enpassant occured")
                else:
                    return "Invalid move"
            else:
                return "Move the {}s".format(col)
        else:
            return "Invalid input"
        return col, st_obj, m, self.c_w_l, self.c_w_r, self.c_b_l, self.c_b_r

    def valid_input(self):
        """Input validation."""
        m = input("Enter a valid input: ")
        while not re.findall(r'[a-h][1-8]-[a-h][1-8]', m) or len(m) != 5:
            print("Invalid input")
            m = input("Enter a valid input: ")
        return m

    def promotion(self, col):
        """Implement promotion as a valid move."""
        self.col = col
        if col == 'white':
            choices = ['N', 'R', 'B', 'Q']
        elif col == 'black':
            choices = ['n', 'r', 'b', 'q']
        else:
            print("no promotion move")
        choice = input("Enter your choice in {}:  ".format(choices))
        while choice not in choices:
            print("Select a valid choice")
            choice = input("Enter your choice in {}:  ".format(choices))
        if choice in ['N', 'n']:
            return Knight(col, choice)
        elif choice in ['Q', 'q']:
            return Queen(col, choice)
        elif choice in ['R', 'r']:
            return Rook(col, choice)
        else:
            return Bishop(col, choice)

    def en_passant(self, st_obj, dest_obj, pre_mv, x, y, x1, y1):
        """Implement enpassant as valid move."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        d = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}
        if pre_mv == 'None':
            return False
        p_x = int(pre_mv[1])-1
        # p_y = d[pre_mv[0]]
        p_x1 = int(pre_mv[4])-1
        p_y1 = d[pre_mv[3]]
        pres = self.board.get((x, n[y])).name
        if self.board.get((p_x1, n[p_y1])) is not None:
            prev = self.board.get((p_x1, n[p_y1])).name
            if all([pres in ['p', 'P'], (abs(p_x-p_x1) == 2),
                    (p_y1 == y1), x1 in [x+1, x-1]]):
                if (pres == 'p' and prev == 'P' and
                   self.board.get((x1+1, n[y1])) is not None):
                    self.board[(x1, n[y1])] = st_obj
                    self.board[(x1+1, n[y1])] = None
                    self.board[(x, n[y])] = None
                    return self.board
                elif (pres == 'P' and prev == 'p' and
                      self.board.get((x1-1, n[y1])) is not None):
                    self.board[(x1, n[y1])] = st_obj
                    self.board[(x1-1, n[y1])] = None
                    self.board[(x, n[y])] = None
                    return self.board
                else:
                    None
            else:
                return False
        else:
            return False

    def check_castling(self, coin, m):
        """Check for check_castling."""
        if coin == 'K':
            self.c_w_l = False
            self.c_w_r = False
        elif coin == 'k':
            self.c_b_l = False
            self.c_b_r = False
        elif coin is 'R' and m[0] is 'a' and m[1] is '1' and self.c_w_l is True:
            self.c_w_l = False
        elif coin is 'R' and m[0] is 'h' and m[1] is '1' and self.c_w_r is True:
            self.c_w_r = False
        elif coin is 'r' and m[0] is 'a' and m[1] is '8' and self.c_b_l is True:
            self.c_b_l = False
        elif coin is 'r' and m[0] is 'h' and m[1] is '8' and self.c_b_r is True:
            self.c_b_r = False
        return self.c_w_l, self.c_w_r, self.c_b_l, self.c_b_r

    def castling_rook_move(self, is_can, name, x, y, x1, y1):
        """Update rook move."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if name in ["K", "k"] and is_can and abs(y-y1) == 2:
            rook_x = 0 if name == "K" else 7
            rook_py = y1+1 if y1 > y else y1-2
            rook_y = y1-1 if y1 > y else y1+1
            self.board[(rook_x, n[rook_y])] = self.board.pop((rook_x, n[rook_py]))

    def legal_moves(self, opt):
        """Find the legal moves for the given piece."""
        d = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        a = int(opt[1]) - 1
        b = opt[0]
        leg_mv = []
        piece = self.board.get((a, b))
        if piece is not None:
            col = piece.color
            for i in range(7, -1, -1):
                for j1 in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
                    j2 = d[j1]
                    if self.board.get((i, j1)) is None:
                        if piece.valid_moves(a, d[b], i, j2, col, self):
                            leg_mv.append(n[j2]+str(int(i)+1))
                    elif self.board.get((i, j1)).color != col:
                        if piece.valid_moves(a, d[b], i, j2, col, self):
                            leg_mv.append(n[j2]+str(int(i)+1))
        return leg_mv


# %%

class Stack():
    """Implement stack."""

    def __init__(self):
        """Stack."""
        self.list = []

    def push(self, state, color=None, opt=None, c_w_l=None, c_w_r=None, c_b_l=None, c_b_r=None):
        """Push function."""
        self.list.append([state, color, opt, c_w_l, c_w_r, c_b_l, c_b_r])

    def pop(self):
        """Pop function."""
        return self.list.pop()

    def top(self):
        """Get the top element."""
        return self.list[-1]


# %%


class Piece:
    """Write a class for piece."""

    def __init__(self, color, name):
        """Write constructor."""
        self.color = color
        self.name = name


class Pawn(Piece):
    """Write a class for pawn."""

    def __init__(self, color, name):
        """Init for Pawn."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate the moves for pawn."""
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        board = chess.board
        a = board.get((x, n[y]))
        b = board.get((x+1, n[y+1]))
        c = board.get((x+1, n[y-1]))
        d = board.get((x-1, n[y+1]))
        e = board.get((x-1, n[y-1]))
        if a.color == 'white':
            if x == 1 and (x1, y1) in [(x+1, y), (x+2, y)]:
                return True
            elif (x1, y1) == (x+1, y):
                return board.get((x+1, n[y])) is None
            elif b is not None and (x1, y1) == (x+1, y+1):
                return b.color != a.color
            elif c is not None and (x1, y1) == (x+1, y-1):
                return c.color != a.color
            else:
                return False
        else:
            if x == 6 and (x1, y1) in [(x-1, y), (x-2, y)]:
                return True
            elif (x1, y1) == (x-1, y):
                return board.get((x-1, n[y])) is None
            elif d is not None and (x1, y1) == (x-1, y+1):
                return d.color != a.color
            elif e is not None and (x1, y1) == (x-1, y-1):
                return a.color != e.color
            else:
                return False


class Rook(Piece):
    """Write a class for rook."""

    def __init__(self, color, name):
        """Init for Rook."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate moves for Rook."""
        board = chess.board
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if x == x1:
            res = []
            if y > y1:
                while y != y1+1:
                    y = y-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res

            else:
                while y != y1-1:
                    y = y+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
        elif y == y1:
            res = []
            if x > x1:
                while x != x1+1:
                    x = x-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
            else:
                while x != x1-1:
                    x = x+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
        else:
            return False


class Knight(Piece):
    """Write a class for Knight."""

    def __init__(self, color, name):
        """Init for Knight."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate the given moves."""
        moves = ([(x+2, y-1), (x+2, y+1), (x+1, y-2), (x+1, y+2), (x-1, y+2),
                 (x-1, y-2), (x-2, y-1), (x-2, y+1)])
        if (x1, y1) in moves:
            return True
        return False


class Bishop(Piece):
    """Write a class for bishop."""

    def __init__(self, color, name):
        """Init for Bishop."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate movmes for Bishop."""
        board = chess.board
        d1 = x-x1
        d2 = y-y1
        n = {0: 'a', 1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e', 6: 'f', 7: 'g',
             8: 'h', 9: 'h'}
        if abs(d1) == abs(d2):
            res = []
            if d1 > 0 and d2 > 0:
                while y != y1+1 and x != x1+1:
                    x = x-1
                    y = y-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
            elif d1 > 0 and d2 < 0:
                while y != y1-1 and x != x1+1:
                    x = x-1
                    y = y+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
            elif d1 < 0 and d2 > 0:
                while y != y1+1 and x != x1-1:
                    x = x+1
                    y = y-1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
            elif d1 < 0 and d2 < 0:
                while y != y1-1 and x != x1-1:
                    x = x+1
                    y = y+1
                    res.append(board.get((x, n[y])) is None)
                return False not in res
        return False


class King(Piece):
    """Write a class for King."""

    def __init__(self, color, name):
        """Init for King."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate the given moves."""
        board = chess.board
        moves = ([(x+1, y-1), (x+1, y), (x+1, y+1), (x, y-1), (x, y+1),
                 (x-1, y-1), (x-1, y), (x-1, y+1)])
        rule1 = (x1, y1) in moves
        rule2 = False
        rule3 = False
        if y1 > y and col == self.color:
            rule3 = all(map(lambda x2: board.get((x, x2)) is None, list('fg')))
        elif y1 < y and col == self.color:
            rule3 = all(map(lambda x2: board.get((x, x2)) is None, list('bcd')))
        if rule3 and abs(y1-y) == 2 and x == x1:
            if self.color == 'white' and y1 > y and col == self.color:
                if chess.c_w_r:
                    rule2 = all([not self.is_check(chess, self.color, x1, y1),
                                 not self.is_check(chess, self.color, x1, y1-1)])
            elif self.color == 'white' and y1 < y and col == self.color:
                if chess.c_w_l:
                    rule2 = all([not self.is_check(chess, self.color, x1, y1),
                                 not self.is_check(chess, self.color, x1, y1+1)])
            elif self.color == 'black' and y1 > y and col == self.color:
                if chess.c_b_r:
                    rule2 = all([not self.is_check(chess, self.color, x1, y1),
                                 not self.is_check(chess, self.color, x1, y1-1)])
            elif self.color == 'black' and y1 < y and col == self.color:
                if chess.c_b_l:
                    rule2 = all([not self.is_check(chess, self.color, x1, y1),
                                 not self.is_check(chess, self.color, x1, y1+1)])
        return rule1 or rule2

    def is_check(self, chess, clr, row=None, column=None):
        """Verify whether the king is in check or not."""
        board = chess.board
        d = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8}
        res1 = []
        res3 = []
        if row is not None and column is not None:
            for key, value in board.items():
                if value is not None and value.color != clr:
                    x = int(key[0])
                    y = d[key[1]]
                    res3.append(value.valid_moves(x, y, row, column, clr, chess))
            return True in res3
        else:
            w_king = 'K'
            b_king = 'k'
            for key, value in board.items():
                if value is not None:
                    if value.name == w_king:
                        w_king_x = int(key[0])
                        w_king_y = d[key[1]]
                    elif value.name == b_king:
                        b_king_x = int(key[0])
                        b_king_y = d[key[1]]
                    else:
                        None
            x, y = (w_king_x, w_king_y) if clr == 'white' else (b_king_x, b_king_y)
            for key, value in board.items():
                if value is not None:
                    if value.color != clr:
                        position_x = int(key[0])
                        position_y = d[key[1]]
                        res1.append(value.valid_moves(position_x, position_y,
                                    x, y, clr, chess))
                    else:
                        None
            return True in res1


class Queen(Piece):
    """Write a class for Queen."""

    def __init__(self, color, name):
        """Init for Queen."""
        Piece.__init__(self, color, name)

    def valid_moves(self, x, y, x1, y1, col, chess):
        """Validate the moves for Queen."""
        r1 = Rook.valid_moves(self, x, y, x1, y1, col, chess)
        r2 = Bishop.valid_moves(self, x, y, x1, y1, col, chess)
        return r1 or r2


# %%
class Game:
    """Game."""

    def __init__(self, x=None):
        """Construct."""
        self.cb = Chess_Board()
        if x is None:
            self.w_k, self.b_k = self.cb.create_board()
        else:
            self.w_k, self.b_k = self.cb.create_board(x)
        self.s = Stack()
        self.n = deque()
        self.left_pieces = []
        self.pieces = self.cb.board.values()
        self.c = 'white'
        self.status = 'Make valid move'
        self.pre_m = None
        self.s.push(self.cb.board.copy(), 'black', None, self.cb.c_w_l,
                    self.cb.c_w_r, self.cb.c_b_l, self.cb.c_b_r )
        self.bk_count = 0
        self.wk_count = 0

    def is_two_king(self):
        """Is two kings present."""
        for i in self.pieces:
            if i is not None:
                self.left_pieces.append(i.name)
        return "K" in self.left_pieces and "k" in self.left_pieces

    def main(self, inp):
        """Write main function."""
        opt = inp
        if ((not re.findall(r'[a-h][1-8]-[a-h][1-8]', opt) or len(opt) != 5)
                and (len(opt) != 1 or not re.findall(r'[1234]', opt))):
            self.status = "Invalide input"
            return self.status, False, self.c
        if opt == '1':
            if len(self.s.list) > 1:
                self.n.append(self.s.pop())
                self.pre_m = self.s.list[-1][2]
                self.status = "Undo"
            else:
                self.status = "cann't undo"
            a = self.s.top()
            self.cb.board, self.c, opt, c_w_l, c_w_r, c_b_l, c_b_r = (a[0].copy(), a[1], a[2], a[3], a[4], a[5], a[6])
            self.cb.c_w_l = c_w_l
            self.cb.c_w_r = c_w_r
            self.cb.c_b_l = c_b_l
            self.cb.c_b_r = c_b_r
            self.c = 'black' if self.c == 'white' else 'white'
            return self.cb.board, self.c, self.status
        else:
            opt = self.redo(opt)
            if opt == '2':
                return "Can't redo", False, self.c
            else:
                output1 = self.cb.move(self.c, opt, self.pre_m)
            if type(output1).__name__ == 'str':
                return output1, False, self.c
            else:
                self.c, st, self.pre_m, c_w_l, c_w_r, c_b_l, c_b_r = output1
        (self.s.push(self.cb.board.copy(), self.c, opt, self.cb.c_w_l,
                     self.cb.c_w_r, self.cb.c_b_l, self.cb.c_b_r))
        king = self.w_k if self.c == 'white' else self.b_k
        if king.is_check(self.cb, self.c):
            self.status = "{} King is checked".format(self.c)
            if len(self.s.list) > 1:
                self.s.pop()
                self.pre_m = self.s.list[-1][2]
            a = self.s.top()
            self.cb.board, self.c, opt, c_w_l, c_w_r, c_b_l, c_b_r = (a[0].copy(), a[1], a[2], a[3], a[4], a[5], a[6])
            self.c = 'black' if self.c == 'white' else 'white'
            return self.status, False, self.c
        else:
            self.c = 'black' if self.c == 'white' else 'white'
        return self.status, True, self.c

    def redo(self, opt):
        """Redo."""
        if opt == "2" and len(self.n) > 0:
            a = self.n.pop()
            opt = a[2]
            self.status = opt
            return opt
        elif opt != 2:
            self.n = deque()
            self.status = "Make move"
            return opt
