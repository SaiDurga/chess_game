"""Chess ui."""

import tkinter as tk
import tkinter.filedialog
import configugurations as cfg
import controller as ctl
from collections import deque
import re
import game as g
from itertools import groupby
from pygame import mixer
path = '/home/nandigamchandu/python/chess_game/sounds/beep-01a.wav'
mixer.init(44100)
d1 = mixer.Sound(path)

# colors
DARK_SQUARE_COLOR = "#DDB88C"
LIGHT_SQUARE_COLOR = "#A66D4F"
controller = ctl.Controller()


class ChessBoard(tk.Frame):
    """Create board."""

    def __init__(self, parent, squareWidth=64, squareHeight=64):
        """Constructor."""
        tk.Frame.__init__(self, parent)
        self.parent = parent
        self.chessState = None
        self.input = tk.StringVar()
        self.squareWidth = squareWidth
        self.squareHeight = squareHeight
        self.width = 8*squareWidth
        self.height = 8*squareHeight
        self.pieceToPhoto = {}
        self.textEntry = None
        self.read_file = []
        self.save_file = []
        self.color = tk.StringVar()
        self.color.set('white move')
        self.status1 = tk.StringVar()
        self.status1.set('Make A Valid Move')
        self.board()
        self.loadImages()
        self.inputStatus()
        self.menu()
        self.game = g.Game()

    def menu(self):
        """Menu for chess."""
        menu_bar = tk.Menu(self.parent)
        file_menu = tk.Menu(menu_bar, tearoff=0)
        about_menu = tk.Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label='File', menu=file_menu, font=("Time", 15))
        menu_bar.add_cascade(label='About', menu=about_menu, font=("Time", 15))
        file_menu.add_command(label="New Game", accelerator='Ctrl+N',
                              compound='left', command=self.clear,
                              font=("Time", 15))
        file_menu.add_command(label="Open", accelerator='Ctrl+O',
                              compound='left', command=self.open_file,
                              font=("Time", 15))
        file_menu.add_command(label="Save", accelerator='Ctrl+S',
                              compound='left', command=self.save,
                              font=("Time", 15))
        file_menu.add_command(label="Save Positions",
                              command=self.save_positions, font=("Time", 15))
        file_menu.add_command(label="Load Positions",
                              command=self.load_positions, font=("Time", 15))
        self.parent.config(menu=menu_bar)

    def open_file(self, event=None):
        """Open file."""
        input_file_name = (tkinter.filedialog.askopenfilename())
        if input_file_name:
            global file_name
            file_name = input_file_name
            with open(file_name) as f:
                data = f.read()
                data = data.splitlines()
            res = []
            for i in data:
                regex = "(?:\d\.)?(?:\s)?([a-h\-1-8]{5})"
                res = res + re.findall(regex, i)
        self.read_file = deque(res)

    def save(self, event=None):
        """Save file."""
        input_file_name = (tkinter.filedialog.asksaveasfile())
        if input_file_name:
            global file_name
            file_name = input_file_name
            input1 = ''
            j = 1
            for i in range(0, len(self.save_file), 2):
                if len(self.save_file[i:i+2]) == 2:
                    x1 = '{}. {} {}\n'.format(j, *self.save_file[i:i+2])
                else:
                    x1 = '{}. {}'.format(j, *self.save_file[i:])
                input1 = input1 + x1
                j += 1
            with open(file_name.name, 'w') as f:
                f.write(input1)
            print(self.save_file)

    def save_positions(self, event=None):
        """Save positions."""
        global file_name
        input_file_name = (tkinter.filedialog.asksaveasfile())
        file_name = input_file_name
        if input_file_name:
            lst = ''
            for i in range(7, -1, -1):
                for j in 'abcdefgh':
                    x = self.game.cb.board.get((i, j))
                    if x is None:
                        x = ' '
                    else:
                        x = x.name
                    lst = lst + x
                lst = lst + '/'
            new_lst = []
            for i, j in groupby(lst, lambda x: x == ' '):
                if i:
                    new_lst.extend([str(len(list(j)))])
                else:
                    new_lst.extend(j)
            s = ''.join(new_lst)
            with open(file_name.name, 'w') as f:
                f.write(s)

    def load_positions(self, event=None):
        """Load positions."""
        input_file_name = (tkinter.filedialog.askopenfilename())
        if input_file_name:
            global file_name
            file_name = input_file_name
            with open(file_name) as f:
                data = f.read()
            x = self.reverse_notation(data)
            self.game = g.Game(x)
            x = controller.construct_from_board(self.game.cb.board)
            self.movePiece(x)

    def digit_space(self, x):
        """Convert digit to spaces."""
        lst = ''
        for i in x:
            if i.isdigit():
                lst = lst + ' '*int(i)
            else:
                lst = lst + i
        return lst

    def reverse_notation(self, x):
        """Reverse notation."""
        lst = x.split('/')
        board = list(map(self.digit_space, lst))
        board = ''.join(board)
        pos = {}
        N = 0
        for i in range(7, -1, -1):
            for j in 'abcdefgh':
                if board[N] != ' ':
                    pos[(i, j)] = board[N]
                N += 1
        return pos

    def board(self):
        """Side labels, board, buttom labels."""
        self.chessboard = tk.Frame(self)
        self.left = tk.Frame(self.chessboard)
        self.board = tk.Frame(self.chessboard)
        self.square = tk.Frame(self.board)
        self.bottom = tk.Frame(self.board)
        self.status = tk.Frame(self)

        self.canvas = tk.Canvas(self.square, width=self.width,
                                height=self.height)
        self.canvas.bind("<Button-1>", self.on_square_clicked)
        self.boardPrint()
        self.square.pack(side='top')
        self.left_frames = [None] * 9
        i = 0
        for label in range(8, 0, -1):
            self.left_frames[i] = tk.Frame(self.left, bd=4)
            (tk.Label(self.left_frames[i], text=label, font=("Times", 16))
               .pack(anchor='center'))
            self.left_frames[i].pack(pady=16)
            i += 1
        self.left_frames[8] = tk.Frame(self.left, bd=2).pack(pady=20)
        self.left.pack(side='left')

        self.bottom_frames = [None] * 8
        i = 0
        for label in 'abcdefgh':
            self.bottom_frames[i] = tk.Frame(self.bottom, bd=5)
            (tk.Label(self.bottom_frames[i], text=label,
                      font=("Times", 16))
               .pack(anchor='center'))
            self.bottom_frames[i].pack(side='left', padx=20)
            i += 1
        self.bottom.pack(side='bottom')
        self.board.pack(side='right')
        self.chessboard.pack(side='top')

    def inputStatus(self):
        """Input and status."""
        (tk.Label(self.status, textvariable=self.color, width=20,
                  font=("Time", 15))
           .grid(row=0, column=50, padx=30, pady=5, columnspan=40, sticky='e'))
        self.warning = (tk.Label(self.status, textvariable=self.status1,
                                 width=30, font=("Time", 15)))
        self.warning.grid(row=1, column=0, padx=1, pady=5, columnspan=40,
                          sticky='e')
        (tk.Label(self.status, text='Enter Input', font=("Time", 15))
           .grid(row=1, column=60, padx=5))
        (tk.Button(self.status, text="prev", command=self.prevMove,
                   font=("Time", 15))
           .grid(row=0, column=20))
        (tk.Button(self.status, text="next", command=self.nextMove,
                   font=("Time", 15))
           .grid(row=0, column=25))
        self.textEntry = tk.Entry(self.status, width=8,
                                  textvariable=self.input)
        self.textEntry.grid(row=1, column=61, padx=2, pady=2, columnspan=64)
        self.textEntry.focus_set()
        self.textEntry.bind("<Return>", self.getInput)
        self.status.pack(side='left')

    def nextMove(self, event=None):
        """Next move."""
        if len(self.read_file) > 0:
            input = self.read_file.popleft()
            st, can, color = self.game.main(input)
            self.inputCheck(input, st, can, color)
        else:
            inp = "2"
            input, can, color = self.game.main(inp)
            self.inputCheck(input, 'Redo', can, color)

    def prevMove(self, event=None):
        """Prevous move."""
        inp = "1"
        x, color, st = self.game.main(inp)
        x = controller.construct_from_board(x)
        self.color.set('{} move'.format(color))
        self.status1.set(st)
        self.movePiece(x)
        self.save_file = self.save_file[:-1]

    def getInput(self, event=None):
        """Refresh."""
        input = self.input.get()
        st, can, color = self.game.main(input)
        self.inputCheck(input, st, can, color)

    def inputCheck(self, input, st, can, color):
        """Check input."""
        if can:
            if len(input) == 5:
                self.save_file.append(input)
                input = input[0:2]+input[3:]
            x = controller.updatePos(input, self)
            self.status1.set(st)
            self.color.set('{} move'.format(color))
            self.movePiece(x)
            self.textEntry.delete(0, 'end')
            self.warning.config(fg='black')
        else:
            if input == "Can't redo":
                st = input
            self.status1.set(st)
            self.color.set('{} move'.format(color))
            self.warning.config(fg='red')
            d1.play()
            self.textEntry.delete(0, 'end')

    def clear(self):
        """Reset board."""
        self.status1.set("White move")
        self.color.set("white move")
        self.canvas.delete('piece')
        self.game = g.Game()
        controller.start_game()
        self.refreshCanvasFromState()
        self.save_file = []

    def movePiece(self, positions):
        """Move piece on board."""
        self.canvas.delete('piece')
        for rname in '12345678':
            for fname in 'abcdefgh':
                sname = fname + rname
                piece = positions.get(sname, ' ')
                if piece != ' ':
                        self.placePieces(sname, piece)

    def boardPrint(self):
        """Print board."""
        for ridx, rname in enumerate(list('87654321')):
            for fidx, fname in enumerate(list('abcdefgh')):
                color = ([LIGHT_SQUARE_COLOR,
                          DARK_SQUARE_COLOR][(ridx-fidx) % 2])
                shade = ['dark', 'light'][(ridx-fidx) % 2]
                tags = [fname+rname, shade, 'square']
                (self.canvas.create_rectangle(
                                fidx*self.squareWidth, ridx*self.squareHeight,
                                fidx*self.squareWidth+self.squareWidth,
                                ridx*self.squareHeight+self.squareHeight,
                                outline=color, fill=color, tag=tags,
                                activefill='#4C4CFF'))
        self.canvas.grid(row=0, column=0)

    def loadImages(self):
        """Load pieace images."""
        for piece in 'pnbrqkPNBRQK':
            self.pieceToPhoto[piece] = (tk.PhotoImage(
                                               file='./images/%s.png' % piece))

    def on_square_clicked(self, event):
        """Click on square."""
        tag = self.get_clicked_row_column(event)
        print(tag)

    def get_clicked_row_column(self, event):
        """Click event."""
        col_size = row_size = 64
        clicked_column = 1 + (event.x // col_size)
        clicked_row = (event.y // row_size)
        tag = self.canvas.gettags(clicked_row * 8 + clicked_column)
        return tag

    def placePieces(self, square, piece):
        """Place pieces."""
        if piece == ' ':
            return

        item = self.canvas.find_withtag(square)
        coords = self.canvas.coords(item)

        photo = self.pieceToPhoto[piece]
        self.canvas.create_image(coords[0], coords[1], image=photo,
                                 state=tk.NORMAL, anchor='nw',
                                 tag='piece')

    def refreshCanvasFromState(self):
        """Set position."""
        for rname in '12345678':
            for fname in 'abcdefgh':
                sname = fname + rname
                piece = cfg.START_PIECES_POSITION.get(sname, ' ')
                if piece != ' ':
                    self.placePieces(sname, piece)


root = tk.Tk()
root.title('Chess Game')
cbt = ChessBoard(root)
cbt.refreshCanvasFromState()
cbt.pack()
root.mainloop()
