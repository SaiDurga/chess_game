"""Use game.py to print on console."""

import game as g

moves = ['a2-a4', 'a7-a5', 'b2-b4', 'b7-b5']

game = g.Game()
for i in moves:
    game.main(i)
    game.cb.print_board()
